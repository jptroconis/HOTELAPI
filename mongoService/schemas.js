Schema = require('mongoose').Schema;

const schemas =  {

    Hotel : {
        key : 'hoteles',
        schema : Schema({
            id        : String,
            name      : String,
            stars     : Number,
            price     : Number,
            image     : String,
            amenities : Array
        })
    }
}; 

module.exports = schemas;
