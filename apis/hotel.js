var dbManager = require('../mongoService/dbManager')
    ,schemas = require('../mongoService/schemas')
    ,fs = require("fs");

const api = 'hotel'    
const methods = {
    listar : 'listar',
    cargar : 'cargarDatos'
}    

function init(app){
    app.get(`/${api}/${methods.listar}`, listar);

}

function listar(req, res){
    let json = req.query.json;
    let data = json ? JSON.parse(json) : {};
    let filtro = {};

    if(data.name){
        filtro.name = new RegExp(data.name , 'i');
    }

    if(data.estrellas && data.estrellas.length > 0){
        filtro.stars = { $in: data.estrellas}
    }

    dbManager.hotel.find(filtro, function(err, docs){
        res.send(docs);
    })
}

module.exports = init
