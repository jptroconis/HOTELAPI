var express = require('express'),
    body_parser = require('body-parser'),
    restful = require('../config/restful'),
    methodOverride = require("method-override")

var app = express();

app.use(body_parser.urlencoded({ extended: false })); 
app.use(body_parser.json());
app.use(methodOverride());
app.use(restful.allowMethods); 

module.exports = app;