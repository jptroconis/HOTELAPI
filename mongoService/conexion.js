const mongoose = require('mongoose');
const conexionDB = require('../config/parametros').mongoDB;

let user = conexionDB.user;
let pass = conexionDB.pass;
let host = conexionDB.host;
let db = conexionDB.db;
let port = conexionDB.port;
'mongodb://username:password@host:port/database?options...'
var conexion = mongoose.createConnection(`mongodb://${user}:${pass}@${host}:${port}/${db}`);

module.exports = conexion