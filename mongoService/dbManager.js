const conexion = require('./conexion');
const schemas = require('./schemas');

const dbManager = {
    hotel : conexion.model(schemas.Hotel.key, schemas.Hotel.schema)
}

module.exports = dbManager;